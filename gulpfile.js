#!/usr/bin/env node

var gulp = require('gulp');
var	ignore = require('gulp-ignore');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('scripts', function() {
	return gulp.src(['www/js/*.js', '!www/js/main.js'])
		.pipe(concat('main.js'))
		.pipe(gulp.dest('www/js/'))
		.pipe(uglify())
		.pipe(gulp.dest('www/js/'));
});

gulp.task('sass', function() {
	return gulp.src('www/css/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('www/css/'));
});

gulp.task('styles', [ 'sass' ], function() {
	return gulp.src(['www/css/*.css', '!www/css/main.css'])
		.pipe(autoprefixer())
		.pipe(concat('main.css'))
		.pipe(gulp.dest('www/css/'))
		.pipe(minify())
		.pipe(gulp.dest('www/css/'));
});

gulp.task('default', [ 'scripts', 'styles' ]);
