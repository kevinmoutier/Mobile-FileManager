var MiogaFileManager = Backbone.Model.extend({

	mioga: null,

	// Browsing
	path: null,
	nodes: null,

	// Copy/cut/paste
	pasteMode: null,
	pasteFiles: null,

	// Create file
	fileType: null,
	fileToUpload: null,

	// Delete files
	deleteFiles: null,

	commentsNode: null,
	comments: null,
	newComment: null,
	newCommentNode: null,
	
	initialize: function(args) {
		this.mioga = args.mioga;

		var that = this;

		this.bind('change:path', function() { that.getFullNodes(); });
		this.bind('change:pasteFiles', function() { that.pasteResources(); });
		this.bind('change:fileToUpload', function() { that.uploadFile(); });
		this.bind('change:deleteFiles', function() { that.deleteResources(); });
		this.bind('change:commentsNode', function() { that.getComments(); });
		this.bind('change:newComment', function() { that.addComment(); });
	},

	pasteResources: function() {
		if (this.get('pasteFiles') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				crossDomain: true,
				data: {
					clipboard: true,
					entries: that.get('pasteFiles'),
					destination: that.get('path'),
					mode: that.get('pasteMode')
				},
				xhrFields: { withCredentials: true },
				traditional: true,
				url: that.mioga.get('private_bin_uri') + '/Magellan/PasteResources.json',
				success: function() {
					that.set('pasteFiles', null);
				},
				error: function(error) {					
					alert('Une erreur est survenue : ' + error);
				}
			});
		}
	},

	uploadFile: function() {
		if (this.get('fileToUpload') != null) {
			// min. IE 10
			var data = new FormData();
			data.append('file', this.get('fileToUpload'));
			data.append('destination', this.get('path'));

			var that = this;
			
			$.ajax({
				type: 'POST',
				url: that.mioga.get('private_bin_uri') + '/Magellan/UploadFile.json',
				crossDomain: true,
				data: data,
				processData: false,
				contentType: false,
				xhrFields: { withCredentials: true },
				success: function() {
					that.set('fileToUpload', null);
				},
				error: function(error) {
					alert('Une erreur est survenue : ' + error);
				}
			});
		}
	},

	getFullNodes: function() {
		var that = this;

		$.ajax({
			type: 'POST',
			url: this.mioga.get('private_bin_uri') + '/Magellan/GetFullNodes.json',
			crossDomain: true,
			xhrFields: { withCredentials: true },
			data: { files: 1, node: that.get('path') },
			success: function(data) {
				that.set('nodes', data); 
			},
			error: function (error) { 
				alert('Une erreur est survenue : ' + error); 
			}
		});
	},

	deleteResources: function() {
		if (this.get('deleteFiles') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				crossDomain: true,
				data: { entries: that.get('deleteFiles') },
				traditional: true,
				xhrFields: { withCredentials: true },
				url: that.mioga.get("private_bin_uri") + "/Magellan/DeleteResources.json",
				success: function(data) {
					that.set('deleteFiles', null);
				},
				error: function(error) {
					alert('Une erreur est survenue : ' + error);
				}
			});
		}
	},

	getComments: function() {
		if (this.get('commentsNode') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				crossDomain: true,
				data: { node: that.get('commentsNode') },
				xhrFields: { withCredentials: true },
				url: that.mioga.get("private_bin_uri") + "/Magellan/GetComments.json",
				success: function(data) {
					that.set('comments', null);
					that.set('comments', data);
				},
				error: function(error) {
					alert('Une erreur est survenue : ' + error);
				}
			});
		}
	},

	addComment: function() {
		if (this.get('newComment') != null && this.get('newCommentNode') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				crossDomain: true,
				data: { node: that.get('newCommentNode'), comment: this.get('newComment') },
				xhrFields: { withCredentials: true },
				url: that.mioga.get("private_bin_uri") + "/Magellan/AddComment.json",
				success: function(data) {
					that.set('newComment', null);
				},
				error: function(error) {
					alert('Une erreur est survenue : ' + error);
				}
			});
		}
	},

	downloadResources: function(entries) {
		if (entries !== undefined && entries.length > 0) {
			var url = this.mioga.get("private_bin_uri") + "/Magellan/DownloadFiles?path=" + encodeURIComponent(entries[0].substr(0, entries[0].lastIndexOf("/")));
			for (var i = 0; i < entries.length; ++i) {
				url += "&entries=" + encodeURIComponent(entries[i]);
			}
			window.location = url;
		}
	}

});
