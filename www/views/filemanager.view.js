var MiogaFileManagerView = Backbone.View.extend({

	$el: null,
	selectMode: false,
	gridView: false,
	selectedFiles: [],
	clipboard: [],

	initialize: function(args) {
		console.log ('[MiogaFileManagerView::initialize]');

		this.$el = args.$el;

		Handlebars.registerHelper('ifBothEmpty', function(a1, a2, options) {
			if (a1 == false && a2 == false) return options.fn(this);
			return options.inverse(this);
		});

		Handlebars.registerHelper('icon', function(key) {
			var iconCode = '';

			if (key == 'directory') iconCode = 'ion-ios-folder';
			else if (key.indexOf('image') > -1) iconCode = 'ion-image';
			else iconCode = 'ion-document-text';

			return new Handlebars.SafeString('<i class="' + iconCode + '"></i>');
		});

		Handlebars.registerHelper('filesize', function(bytes) {
			var sizes = ['octets', 'Ko', 'Mo', 'Go', 'To'];
			if (bytes == 0 || bytes == 1) return bytes + ' octet';
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1000)));
			return Math.round((bytes / Math.pow(1000, i) + 0.00001) * 10) / 10 + ' ' + sizes[i];
		});

		Handlebars.registerHelper('datetime', function(key) {
			var d = new Date(key.replace(/-/g,'/'));
			return	('0' + d.getDate()).slice(-2) + '/' + ('0' + (d.getMonth() + 1)).slice(-2) + '/' + 
					d.getFullYear() + ' à ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
		});

		var that = this;

		this.model.bind('change:nodes', function() { that.renderNodes(); });
		this.model.bind('change:pasteFiles', function() { that.afterPaste(); });
		this.model.bind('change:fileToCreate', function() { that.afterCreate(); });
		this.model.bind('change:fileToUpload', function() { that.afterUpload(); });
		this.model.bind('change:deleteFiles', function() { that.afterDelete(); });
		this.model.bind('change:comments', function() { that.commentsLoaded(); });
		this.model.bind('change:newComment', function() { that.afterComment(); });

		// Register routes for module
		this.model.get('mioga').router.route('filemanager', function() {
			that.boot();
			that.navigate(that.model.get('mioga').get('home_uri'));
		});

		this.model.get('mioga').router.route('filemanager/*path', function(path) {
			if (!$('body').hasClass('main')) that.boot();
			that.navigate(path);
		});
	},

	boot: function() {
		var that = this;

		$('#back').on('click', function() {
			// This has to be changed in order to implement copy/paste
			that.endSelect();
			var path = that.model.get('path');
			path = path.substring(0, path.lastIndexOf('/'));
			that.model.get('mioga').router.navigate('filemanager/' + path, { trigger: true });
		});

		/* $('#btnLogout').on('click', function() {
			that.model.get('mioga').router.navigate('logout', { trigger: true });
		}); */

		$('#new').on('click', function() {
			$('#newFolder, #newFile').toggleClass('shown');
		});

		$('#newFile').on('click', function() { 
			$('#file').click(); 
		});

		this.showNewControls();

		$('#file').on('change', function() {
			if ($('#file').val()) {
				$('.loader').show();
				that.model.set('fileToUpload', $('#file')[0].files[0]);
			}
		});

		$('#tb-download').on('click', function() { that.model.downloadResources(that.selectedFiles); });
		$('#tb-delete').on('click', function() { that.model.set('deleteFiles', that.selectedFiles); });

		$('#viewstyle').on('click', function() {
			var el = $('#viewstyle');
			if (el.attr('class') == 'ion-ios-grid-view-outline') {
				el.attr('class', 'ion-ios-list-outline');
			} else {
				el.attr('class', 'ion-ios-grid-view-outline');
			}
			$('ul.list').toggleClass('grid');
			that.gridView = !that.gridView;
		});

		$('body').addClass('main');
	},

	navigate: function(path) {
		$('.loader').show();

		if (typeof path !== 'undefined') {
			$('#headerText').html(path.split('/').slice(-1).pop());
			this.model.set('path', path);
			// Hide new folder and new file buttons
			$('#newFolder, #newFile').removeClass('shown');
		} else {
			// Trigger the change event even if the value did not change
			this.model.trigger('change:path', this.model);
		}
	},

	selectFile: function(el) {
		var eli = $(el).find('div.info i');

		if (eli.attr('class') == 'ion-ios-circle-outline') {
			eli.attr('class', 'ion-ios-checkmark');
		} else {
			eli.attr('class', 'ion-ios-circle-outline');
		}

		if (this.selectedFiles.indexOf(el.id) > -1) {
			var index = this.selectedFiles.indexOf(el.id);
			this.selectedFiles.splice(index, 1);
		} else {
			this.selectedFiles.push(el.id);
		}

		if (this.selectedFiles.length == 0) {
			this.endSelect();
		}
	},

	hideNewControls: function() {
		$('#newFolder, #newFile').removeClass('shown');

		setTimeout(function() {
			$('#newFolder, #newFile').hide();
			$('#new').addClass('disappear');
		}, 250);
	},

	showNewControls: function() {
		$('#new').removeClass('disappear');

		setTimeout(function() {
			$('#newFolder, #newFile').show();
		}, 250);
	},

	startSelect: function() {
		this.selectMode = true;
		$('#mainView li div.info').html('<i class="ion-ios-circle-outline"></i>');
		this.hideNewControls();
		setTimeout(function() { $('section.toolbar').addClass('shown'); }, 250);
	},

	endSelect: function() {
		this.selectMode = false;
		this.selectedFiles = [];
		$('#mainView li div.info').html('<i class="ion-information-circled"></i>');
		$('section.toolbar').removeClass('shown');
		var that = this;
		that.showNewControls();
	},

	copyResources: function(cut) {
		/*this.clipboard = this.selectedFiles;

		$('#paste').css('opacity', '1');

		var that = this;
		$('#paste').off().on('click', function() {
			$('.loader').show();
			that.model.set('pasteMode', (cut ? 'cut' : 'copy'));
			that.model.set('pasteFiles', that.clipboard);
		});*/
	},

	afterPaste: function() {
		/*if (this.model.get('pasteFiles') == null) {
			if (this.model.get('pasteMode') == 'cut') {
				$('#paste').css('opacity', '0.5');
				$('#paste').off();
				this.clipboard = [];
			}
			// Refresh
			this.navigate();
		}*/
	},

	afterDelete: function() {
		if (this.model.get('deleteFiles') == null) {
			this.endSelect();
			this.navigate();
		}
	},

	afterCreate: function() {
		if (this.model.get('fileToCreate') == null) {
			this.navigate();
		}
	},

	afterUpload: function() {
		if (this.model.get('fileToUpload') == null) {
			this.navigate();
		}
	},

	showInfoView: function(el) {
		var $el = $(el);

		var template = Handlebars.compile($('#infoView').html());

		$('div.infoView').html(template({
			"path": this.model.get('path'),
			"parent": this.model.get('path').split('/').slice(-1).pop(),
			"text": $el.find('div.name').text(),
			"author": $el.attr("data-author"),
			"mimetype": $el.attr("data-mimetype"),
			"size": $el.attr("data-size"),
			"modified": $el.attr("data-modified")
		}));

		$('.tabSelector > div').on('click', function() {
			$('.tab').hide();
			$('.tabSelector > div').removeClass('selected');
			$('#tab-' + $(this).data('target')).show();
			$(this).addClass('selected');
		});

		$('#tab-' + $('.tabSelector > div.selected').data('target')).show();

		$('#close').on('click', function() {
			$('div.infoView').removeClass('visible');
			window.setTimeout(function() {
				$('div.infoView').css('z-index', -1000);
			}, 500);
		});

		if ($el.attr('data-comments') > 0) {
			// Backbone garbage
			this.model.set('commentsNode', null);
			this.model.set('commentsNode', el.id);
		}

		var that = this;

		$('#send-comment').on('click', function() {
			$('.loader').show();
			that.model.set('newCommentNode', el.id);
			that.model.set('newComment', $('#comment').val());
		});

		$('div.infoView').css('z-index', 1000).addClass('visible');
	},

	afterComment: function() {
		this.model.set('commentsNode', null);
		this.model.set('commentsNode', this.model.get('newCommentNode'));
	},

	commentsLoaded: function() {
		if (this.model.get('comments') != null) {
			var template = Handlebars.compile($('#comments').html());
			$('div#comments-container').html(template(this.model.get('comments')));
			$('#comment').val('');
			$('.loader').hide();
		}
	},

	renderNodes: function() {
		var template = Handlebars.compile($('#filemanager-form').html());

		var directories = [], files = [];
		this.model.get('nodes').resources.forEach(function(resource) {
			if (resource.mimetype == "directory") {
				directories.push(resource);
			} else {
				files.push(resource);
			}
		});

		// Render template
		var $node = $(template({ directories: directories, files: files }));
		this.$el.empty().append($node);

		if (this.gridView)
			$('ul.list').addClass('grid');

		var that = this;

		var timer;
		var clickEventType = ((document.ontouchstart !== null) ? ['mousedown', 'mouseup'] : ['touchstart', 'touchend']);
		var moved;

		$('section.view li').on(clickEventType[0], function() {
			var el = this;
			timer = setTimeout(function() {
				if (!moved) {
					if (!that.selectMode) {
						that.startSelect();
					}
					that.selectFile(el);
					moved = true; // avoid selecting two times
				}
			}, 500);
			moved = false;
		}).on(clickEventType[1], function(event) {
			event.preventDefault();	// otherwise it clicks on a tab in the info view
			clearTimeout(timer);

			if (!moved) {
				if (!that.selectMode) {
					// if we click on the information circle
					if ($(event.target.parentNode).hasClass('info')) {
						that.showInfoView(this);
						return;
					}

					if (this.className == 'directory') {
						that.model.get('mioga').router.navigate('filemanager/' + this.id, { trigger: true });
					} else {
						that.showInfoView(this);
					}
				} else {
					that.selectFile(this);
				}
			}
		}).on('touchmove', function() {
			moved = true;
		});

		$('section.view').off('scroll').on('scroll', function() {
			moved = true;
		});

		$('.loader').hide();
	}

});
